FROM ubuntu:20.04
ARG DEBIAN_FRONTEND=noninteractive

SHELL ["/bin/bash", "-c"]
RUN apt-get update && apt-get install -y \
	gcc \
	g++ \
	build-essential \
#	libtool \
#	autoconf \
#	unzip \
#	wget \
#	libssl-dev \
	python3.8 \
	python3-biopython \
    python3-pip \
    git \
	nano \
#	libomp-dev \
#	libopenblas-dev \
#	liblapack-dev \
#	swig \
	&& apt clean all
	

#ENV VERSION 3.21
#ENV BUILD 4
## don't modify from here
#RUN mkdir temp \
#    && cd temp \
#	&& wget https://cmake.org/files/v$VERSION/cmake-$VERSION.$BUILD.tar.gz \
#	&& tar -xzvf cmake-$VERSION.$BUILD.tar.gz \
#	&& cd cmake-$VERSION.$BUILD/ \
#    && ./bootstrap \
#	&& make -j$(nproc) \
#	&& make install \
#	&& cd ../..

#RUN cmake --version \
#    && rm -r temp
RUN pip install Scikit-learn numpy faiss-gpu pandas

## Maybe try to install the nvcc and cuda libraries

#RUN git clone https://github.com/facebookresearch/faiss.git faiss \
#    && cd faiss \
#	&& cmake -B build . \
#    -DFAISS_ENABLE_GPU=OFF \
#    -DFAISS_ENABLE_PYTHON=ON \
#    -DBUILD_SHARED_LIBS=ON \
#    -DBUILD_TESTING=ON \
#	&& make -C build -j faiss \
#    && make -C build -j swigfaiss \
#	&& cd build/faiss/python \
#	&& python3 setup.py install \
#	&& cd ../../.. \
#	&& make -C build install \
#	&& make -C build test \
#	&& cd .. \
#	&& rm -r faiss

RUN git clone https://bitbucket.org/jspagnuolo/giana.git giana \
    && python3 ./giana/GIANA4.1.py -f ./giana/data/tutorial.txt